# Автоматическое разворачивание стека LEMP с Wordpress 

Автоматическое разворачивание стека LEMP с Wordpress
Стек такой: Vagrant, nginx, mariadb, php, wordpress, Ansible
Архитектура: 
1) vagrant1 - nginx, php, wordpress (1 нода)
2) vagrant2 - общая бд для 3 нод: mariadb
3) vagrant3 - nginx, который балансирует на 3 ноды.
4) vagrant4 - nginx, php, wordpress (2 нода)
5) vagrant5 - nginx, php, wordpress (3 нода)
Данные действия автоматизированны с помошью provision Vagrant, для этого используется Ansible.
